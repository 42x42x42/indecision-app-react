import React from 'react';
import OneOption from './OneOption';

const Options = props => {
    return <div>

        <div className="widget-header">
            <h3 className="widget-header__title">Your options</h3>
            <button
                className="btn btn--link"
                onClick={props.handleDeleteOptions}>Remove all
            </button>
        </div>

        {props.options.length === 0 && <p className="widget__error">Please add an option</p>}
        {props.options.map((option, index) => (
            <OneOption
                key={option}
                optionText={option}
                count={index + 1}
                handleDeleteSingleOption={props.handleDeleteSingleOption}
            />))}
    </div>;
};

export default Options;