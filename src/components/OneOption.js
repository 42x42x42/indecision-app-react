import React from 'react';

 const OneOption = props => {
    return (
        <div className="option">
            <p className="option__text">{props.count}. {props.optionText}</p>
            <button
                className="btn btn--link"
                // onClick={props.handleDeleteSingleOption} // тоже рабочий вариант, передает кликнутый элемент,а потом находится значение его соседа
                onClick={(e) => props.handleDeleteSingleOption(props.optionText)}
            > remove
            </button>
         </div>)
};

export default OneOption;