import React from 'react';

export default class AddOption extends React.Component {
    constructor(props) {
        super(props);
        this.handleAddOption = this.handleAddOption.bind(this);//всегда писать когда есть вызов в функции this
        this.state = {
            error: undefined
        };
    }

    handleAddOption(e) {
        e.preventDefault();

        const value = e.target.elements.addInput.value.trim(); // trim delete spaces at start end end of a string
        let error = this.props.handleAddOption(value);
        //{error} === {error: error} + короткая запись функции с =>({object})
        this.setState(() => ({error}));

        if(!error){
            e.target.elements.addInput.value='';
        }
    }

    render() {
        return (<div>
                {this.state.error && <p className="add-option-error">{this.state.error}</p>}
                <form className="add-option" onSubmit={this.handleAddOption}>
                    {/*name of elements is obligatory to get it's value*/}
                    <input className="add-option__input" type="text" name="addInput"/>
                    <button action="submit" className="btn"> Add Option</button>
                </form>
            </div>
        )
    };
}