import React from 'react';

const Action = (props) => {
    return <button
        className="btn-big"
        onClick={props.handlePick}
        disabled={!props.hasOptions}>
        What should I do?
    </button>
};

export default Action;

// ACTION AS USUAL COMPONENT
// class Action extends React.Component {
//     render() {
//         return <button
//             onClick={this.props.handlePick}
//             disabled={!this.props.hasOptions}>
//             What should I do?
//         </button>
//     };
// }
