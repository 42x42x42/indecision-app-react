import React from 'react';
import AddOption from './AddOption';
import Options from './Options';
import Action from './Action';
import Header from './Header';
import OptionModal from './OptionModal';

class IndecisionApp extends React.Component {
    constructor(props) {
        //прокидывает пропс в родительский класс чтоб сохранить его логику работы
        super(props);
        //для каждого метода сделан bind(this) чтоб сохранить контекст и оставить доступ к общему state
        this.handlePick = this.handlePick.bind(this);
        this.handleCloseSelected = this.handleCloseSelected.bind(this);
        this.handleDeleteOptions = this.handleDeleteOptions.bind(this);
        this.handleDeleteSingleOption = this.handleDeleteSingleOption.bind(this);
        this.handleAddOption = this.handleAddOption.bind(this);
        //к state родителя есть доступ у всех компонентов и можно делать ре-рендеринг через setState, пропс передается только от родителя вниз к child
        this.state = {
            options: props.options,
            selectedOption: undefined
        };
    };

    componentDidMount() {
        try {
            const json = localStorage.getItem('options');
            const options = JSON.parse(json);
            if (options) {
                this.setState(() => ({options: options}));
            }
        } catch (e) {
            console.error(e);
        }

    };

    componentDidUpdate(prevProps, prevState) {
        if (prevState.options.length !== this.state.options.length) {
            const json = JSON.stringify(this.state.options);
            localStorage.setItem('options', json);
        }
    };

    handlePick() {
        // choose random option
        const selected = this.state.options[Math.floor(Math.random() * this.state.options.length)];
        //setState with object in arguments,not function as usual
        this.setState({
            selectedOption: selected
        })
    }

    handleCloseSelected() {
        this.setState({
            selectedOption: false
        })
    }


    handleDeleteOptions() {
        this.setState(() => ({options: []}))
    };

    //прокидывается через прос Option => OneOption =>onClick и влияет на общее состояние приложения т.к. объявлена и bind к this IndecisionApp
    handleDeleteSingleOption(optionToRemove) {
        // console.log('hdso ', option.target.parentNode.firstChild.innerText); \\ вариант один, если передается просто элемент на который кликнули(кнопка удалить)
        this.setState((prevState) => ({
            options: prevState.options.filter((option) => {
                return optionToRemove !== option
            })
        }))
    };

//  // длинная запись ниже, выше короткая, нужно обязательно после стрелки => брать в скобки () объект {}
//  // т.к. иначе {} воспринимает как просто скобки внутри которых код ДЛЯ исполнения,а не объект

    // handleDeleteOptions() {
    //     //delete all options
    //     this.setState(() => {
    //         return {
    //             options: []
    //         };
    //     });
    // };


    handleAddOption(option) {
        //add new option to a list

        if (!option) {
            return 'Please enter a valid value';
        }
        else if (this.state.options.indexOf(option) > -1) {
            return 'This value already in a list';
        }
        //коротка запись с ({возвращаемый ОБЪЕКТ}). () обязательно оборачивают объект
        this.setState(prevState => ({options: prevState.options.concat(option)}));
    };

    /*--------------------------
    -------MAIN PAGE RENDER-----
    ---------------------------*/
    render() {
        const subtitle = 'Put your life in a hands of a computer';
        return (
            <div>
                <Header subtitle={subtitle}/>
                <div className="container">
                    <Action
                        hasOptions={this.state.options.length > 0}
                        handlePick={this.handlePick}
                    />

                    <div className="widget__main">
                        <Options
                            options={this.state.options}
                            handleDeleteOptions={this.handleDeleteOptions}
                            handleDeleteSingleOption={this.handleDeleteSingleOption}
                        />
                        <AddOption
                            handleAddOption={this.handleAddOption}
                        />
                    </div>

                </div>

                <OptionModal
                    selectedOption={this.state.selectedOption}
                    handleCloseSelected={this.handleCloseSelected}

                />
            </div>
        );
    };
}

IndecisionApp.defaultProps = {
    options: []
};

// если заданы defaultProps то можно прокидывать при объявлении компонента в него пропсы
// ReactDOM.render(<IndecisionApp options={[1,2,3]}/>, appRoot);

export default IndecisionApp;