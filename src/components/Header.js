import React from 'react';

const Header = props => {
    return (
        <div className="header">
            <div className="container">
                {/*в statless component обращение к props идет без this т.к. они передаются в аргументе(через атрибуты элемента при вызове)*/}
                <h1 className="header__title">{props.title}</h1>
                {/*если пропс.сабтайтл существует, то отрендерить его, если нет,то никакой элемент не добавится*/}
                {props.subtitle && <h2 className="header__subtitle">{props.subtitle}</h2>}
            </div>

        </div>
    );
};
//задает значение переменных в props по умолчанию
Header.defaultProps = {
    title: "Indecision app"
};

export default Header;