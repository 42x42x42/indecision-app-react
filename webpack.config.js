const path = require('path');

module.exports = {
    entry: './src/app4-webpack.js',
    output: {
        path: path.join(__dirname, 'public'),
        filename: "bundle.js"
    },
    module: {
        rules: [{
            loader: 'babel-loader',
            test: /\.js$/,
            exclude: /node_modules/
        },
            {
                test: /\.s?css$/,
                use:[
                    'style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            }]
    },
    devtool :'cheap-module-eval-source-map',
    devServer :{
        contentBase: path.join(__dirname, 'public')
    }
};

//module настраивает трансформацию через бабель перед тем как вебпак создаст bundle.js

// devtool исправляет отображение ошибок в консоли хроме и делает ссылку на оригинальный код,а не bundle.js
// >> 'cheap-module-eval-source-map' Для разработки

/* настройка девсерва, нужно так же сделать команду в Package json, путь указывается к файлу index.html, path импортируется
devServer :{
contentBase: path.join(__dirname, 'public')
}
 devServer не генерирует физически файл bundle.js он хранит все в оперативной памяти
*/

//babel-plugin-transform-class-properties позволяет избежать необходимости
// в конструкторе писать this.eventHandler = this.eventHandler.bind(this),
// можно сразу через foo = (e) => {..} писать в классе без конструктора и this всегда будет объект в котором функция объявлена
// переменные в классе можно писать без типа let\var а сразу так: name = 'Mike' . это равнозначно в конструкторе this.name='Mike

// css-loader загружает css в js script
// style-loader Загружате стиль в каждый элемент в дом в тег style
// в настройках нужно добавить код отдельным объектом в раздел модули-> rules
//useпозволяет указать сразу несколько лоадеров
// сам css файл нужно просто импортировать в файл с финальным компонентом(app) import './styles/styles.scss';
/*
{
    test: /\.css$/,
        use:[
    'style-loader',
    'css-loader'
]
}]
*/


/* SCSS нужно установить sass-loader node-sass
нужно в test указать файл .scss
Знак вопроса /\.s?css$/ означает что будут браться в работу css и scss файлы. ? означает необязательность предыдущего символа
{
                test: /\.s?css$/,
                use:[
                    'style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            }]

 */

//npm install normalize.css нормализирует css
// import 'normalize.css/normalize.css'; в основном файле приложения + см выше настройки вебпак конфига. Не забыть перезапустить сервер